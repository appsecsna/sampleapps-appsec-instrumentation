/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sample.data.jpa.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import sample.data.jpa.service.CityService;

@Controller
public class SampleController {

    @Autowired
    private CityService cityService;

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/")
    @ResponseBody
    @Transactional(readOnly = true)
    public String helloWorld() {
        return this.cityService.getCity("Bath", "UK").getName();
    }
    @GetMapping("/app2")
    @ResponseBody
//	@Transactional(readOnly = true)
    public String connectApp2() {

        try {
            this.restTemplate.getForEntity("http://localhost:8081/api/cities", String.class);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        //return this.cityService.getCity("Bath", "UK").getName();
        return "this is app1 I have made a rest call to app2";
    }

}
